import { Component, OnInit } from '@angular/core';

import * as FusionCharts from 'fusioncharts';
import { Observable } from "rxjs/Rx";
import { ActivatedRoute } from '@angular/router';
import { DataService } from 'app/services/data.service';
@Component({
  selector: 'app-accessi',
  templateUrl: './accessi.component.html',
  styleUrls: ['./accessi.component.css']
})
export class AccessiComponent implements OnInit {
  datitot:any;
  report:any;
  filtroTempo="this month";
  canale="";
  dataSource2:any;
  dataSource:any;
  width = 600;
  height = 250;
  type = 'pie3d';
  type2='column2d'
  dataFormat = 'json';
  events:any;
  selettori:any[]=[];
  
 constructor(private dataService:DataService, private route:ActivatedRoute) { 

    console.log(route.snapshot.data[0].pagina);
    this.report=route.snapshot.data[0].pagina;
    if (this.report=="TroubleTicket") {
      this.filtroTempo="totali"
    }

    dataService.getTotali(this.report,this.filtroTempo).subscribe(datitot=>{
      for (var index = 0; index < datitot.length; index++) {
        this.selettori[index] = datitot[index].label;
        
      }
      dataService.getAllDetails(this.report, this.selettori, this.filtroTempo);
      this.datitot=datitot;
      dataService.getMedio(this.datitot);
     this.dataSource = {
  "chart": {

      "startingangle": "120",
      "showlabels": "1",
      "showlegend": "1",
     
      "slicingdistance": "15",
      "showpercentvalues": "0",
      "showpercentintooltip": "0",
      "plottooltext": "$label",
      "theme": "ocean",
      
      "bgColor": "ffa726,fb8c00",
      "bgratio": "60,40",
      "bgAlpha": "70,80",
      "canvasPadding": "0",
      "showLegend":"0",
      "valuePadding":"2",
      "usePlotGradientColor": "1",
      "showshadow": "1",
      "plotGradientColor": "#003366",
      "labelDisplay": "wrap",
      "chartLeftMargin": "0",        
      "chartTopMargin": "0",        
      "chartRightMargin": "0",        
      "chartBottomMargin": "0",
    
  },
  "data":this.datitot
};

this.events = {
  slicingStart: (eventObj, dataObj) => {
      this.canale=dataObj.data.toolText;
      console.log(dataObj.data);
      

      this.dataSource2 = {
        "chart": {
      
          "canvasBgAlpha": "0",
          "bgColor": "ef5350,e53935",
          "bgratio": "60,40",
          "bgAlpha": "70,80",
          "theme": "fint",
          "plotHoverEffect":"1",
          "plotFillHoverAlpha":"60",
          "plotFillHoverColor":"#FFFFFF",
          "showValues": "1",
          "valueFontColor":"#00000",
          "divLineColor":"#FFFFFF",
          "yFormatNumber":"0",
          "forceDecimals":"1",
          "paletteColors":"#00000"
        
        },
        "data": dataService.getVal(this.selettori.indexOf(dataObj.data.toolText)),
        
        "trendlines": [
          {
              "line": [
                  {
                      "startvalue": dataService.getMedio(dataService.getVal(this.selettori.indexOf(dataObj.data.toolText))),
                      "color": "#1aaf5d",
                      "valueOnRight": "1",
                      "tooltext": "Quarterly sales target was $startDataValue",
                      "displayvalue": "valore medio"
                  }
              ]
          }
      ]
      };
     
  }
  
}

    });
  };
      
  ngOnInit() {
   //this.dataService.getTotali("accessi").map(this.datitot=>this.datitot);
  
  /*this.dataSource = {
  "chart": {

      "startingangle": "120",
      "showlabels": "1",
      "showlegend": "1",
     
      "slicingdistance": "15",
      "showpercentvalues": "0",
      "showpercentintooltip": "0",
      "plottooltext": "$label",
      "theme": "ocean",
      
      "bgColor": "ffa726,fb8c00",
      "bgratio": "60,40",
      "bgAlpha": "70,80",
      "canvasPadding": "0",
      "showLegend":"0",
      "valuePadding":"2",
      "usePlotGradientColor": "1",
      "showshadow": "1",
      "plotGradientColor": "#003366",
      "labelDisplay": "wrap",
      "chartLeftMargin": "0",        
      "chartTopMargin": "0",        
      "chartRightMargin": "0",        
      "chartBottomMargin": "0",
    
  },
  "data":this.datitot$=this.dataService.getTotali("accessi")
};
this.events = {
  slicingStart: (eventObj, dataObj) => {
      this.canale=dataObj.data.toolText;
      console.log(dataObj.data.toolText);
      if(dataObj.data.toolText=="Sito"){
        //this.datiFiltro=datiSito;
        
      }
      else{
        //this.datiFiltro=datiApp;
      }
      this.dataSource2 = {
        "chart": {
      
          "canvasBgAlpha": "0",
          "bgColor": "ef5350,e53935",
          "bgratio": "60,40",
          "bgAlpha": "70,80",
          "theme": "fint",
          "plotHoverEffect":"1",
          "plotFillHoverAlpha":"60",
          "plotFillHoverColor":"#FFFFFF",
          "showValues": "1",
          "valueFontColor":"#00000",
          "divLineColor":"#FFFFFF",
          "yFormatNumber":"0",
          "forceDecimals":"1",
          "paletteColors":"#00000"
        
        },
        "data": this.datiFiltro
      };
        
  }
}*/
  }

  
 
  cambiaFiltro(filtro){
    this.filtroTempo=filtro;
    console.log("report",this.report);
    this.dataService.getTotali(this.report,this.filtroTempo).subscribe(datitot=>{
      this.selettori=[];
          for (var index = 0; index < datitot.length; index++) {
        this.selettori[index] = datitot[index].label;
        console.log("report",datitot[index].label);
      }
      this.dataService.getAllDetails(this.report, this.selettori, this.filtroTempo);
      this.datitot=datitot;
      this.dataSource.data=datitot;
      //this.dataSource2.data="";
    })

  }
    



}
