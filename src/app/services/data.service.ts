import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/catch' ;
import 'rxjs/add/operator/map' ;
import 'rxjs/add/observable/throw';
import { forkJoin } from "rxjs/observable/forkJoin";
@Injectable()
export class DataService {
     //url="http://localhost:3005/risorse/";
     url="https://timbusinesskpi.azurewebsites.net/risorse/";
     allVal:any[]=[];
    constructor(private http:Http) { }

    getTotali(risorsa,timerange){
        return this.http.get(this.url+risorsa+'?selector=totali&timerange='+timerange)
        .map(response=>response.json());

    }
    getAllDetails(risorsa, selettori, timerange){
        var chiamate:any[]=[];
        var totalone:any[]=[]
        for (var i = 0; i < selettori.length; i++) {
            console.log("entro");
             chiamate[i] = this.getDettaglio(risorsa,selettori[i],timerange);
             
            
        }
        
        //var chiamata1=this.http.get(this.url+'accessi?selector=app&timerange=this month');
        //var chiamata2=this.http.get(this.url+'accessi?selector=HP&timerange=this month')
        forkJoin(chiamate).subscribe(results => {
            console.log(results);
            this.allVal=results;
            
    });

    }


        getDettaglio(risorsa,selettore,timerange){
      return this.http.get(this.url+risorsa+'?selector='+selettore+'&timerange='+timerange)
        .map(response=>response.json());
    }
    getVal(indice){
        return this.allVal[indice];
    }
    getMedio(oggetto){
        console.log(oggetto);
        var mediovett=[];
        var totale=0;
        for (var index = 0; index < oggetto.length; index++) {
            totale+=parseInt( oggetto[index].value);
            
        }
        
       /* for (var index = 0; index < oggetto.length; index++) {
            
            mediovett[index]=JSON.parse('{"value":"'+totale/oggetto.length+'"}');
            
        }*/
        console.log(mediovett);
        return (totale/oggetto.length);
    }
}