import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import * as $ from 'jquery';
import * as FusionCharts from 'fusioncharts';
import { Observable } from "rxjs/Rx";
import { DataService } from 'app/services/data.service';
@Component({
    selector: 'app-grafico',
    templateUrl: './grafico.component.html',
    
  })
export class GraficoComponent implements OnInit{
    @Input()
       set type(type: string){}
    @Input()
    set report(report: string){console.log(report);}
    @Input()
    set bgcolor(bgcolor: string){}
    datitot:any;
    //report:any;
    filtroTempo="last week";
    canale="";
    //dataSource2:any;
    dataSource:any;
    width = 600;
    height = 180;
    dataFormat = 'json';
    events:any;
    tipoGrafico;
    constructor(private dataService:DataService){
     



    }
    ngOnInit(){
      


    }
    ngOnChanges(changes: SimpleChanges) {
        console.log(changes.report);
        var bg;
        if(changes.report.currentValue!=undefined){
            if (changes.report.currentValue=="TroubleTicket") {
                this.filtroTempo="totali"
              }
            this.dataService.getTotali(changes.report.currentValue,this.filtroTempo).subscribe(datitot=>{
                this.datitot=datitot;
                this.tipoGrafico=changes.type.currentValue;
                 bg=changes.bgcolor.currentValue;   
                this.dataSource = {
            "chart": {
          
                "startingangle": "120",
                "showvalues": "0",
                "showlabels": "0",
                "showlegend": "1",
                "centerLabel": "$label: $value",
                "theme": "fint",
                "slicingdistance": "15",
                "showpercentvalues": "0",
                "showpercentintooltip": "0",
                "plottooltext": "$label",
                "legendItemFontSize":"7",
                
                "bgColor": bg,
                "bgratio": "60,40",
                "bgAlpha": "70,80",
                "canvasPadding": "0",
                "centerLabelFontSize":"7",
                "labelFontBold":"0",
                "valuePadding":"2",
                "usePlotGradientColor": "1",
                "showshadow": "1",
                "plotGradientColor": "#003366",
                "chartLeftMargin": "0",        
                "chartTopMargin": "0",        
                "chartRightMargin": "0",        
                "chartBottomMargin": "0",
              
            },
            "data":this.datitot
          };
      
              });
              

        }
   
    }
    ngAfterViewChecked(){
        $("[class$=creditgroup]").remove();
    }

}