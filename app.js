//dependencies
const express = require('express');  
const path = require('path');  
const http = require('http');  
const bodyParser = require('body-parser');
const fileUpload = require('express-fileupload');
var formidable = require('formidable');
var fs = require('fs');
var proxy = require('express-http-proxy');
//routes
//const api = require('./app/routes/api');
const risorse = require('./risorse');

const app = express();

//parsers for post data
app.use(bodyParser.json());  
app.use(bodyParser.urlencoded({ extended: false }));
app.use(fileUpload());
//static path
app.use(express.static(path.join(__dirname, 'dist')));
app.use(function (req, res, next) {
    
        // Website you wish to allow to connect
        //res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');
        res.setHeader('Access-Control-Allow-Origin', '*');
        // Request methods you wish to allow
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  
        res.header("Access-Control-Allow-Headers", "X-Requested-With, Content-Type");
        next();
    });
//app.use('/api', api);
app.use('/risorse', risorse);
app.use('/mbi', proxy('mbi.tim.it',{
    https: true,
    proxyReqPathResolver: function(req) {
        console.log(req.url);
        console.log(require('url').parse(req.url).path);
        return require('url').parse(req.url).path;
            
      }
  }));
app.get('/upload', function(req, res) {
    console.log(__dirname);
    
    res.sendFile(path.join(__dirname + '/fileUploader/upload.html'));

});
app.get('/configApp', function(req, res) {
    console.log(__dirname);
    
    res.sendFile(path.join(__dirname + '/fileUploader/modifyConfig.html'));

});
app.post('/configApp', function(req, res) {
    console.log(req.body);
    if (req.body!=undefined) {
        var obj=req.body;
        fs.writeFile(path.join(__dirname + '/stub/appConfig.json'),JSON.stringify(obj), 'utf8',function (err) {
            if (err) throw err;
            console.log('Saved!');
            res.send('saved');
          });    }
});
app.post('/upload', function(req, res) {
    if (!req.files)
      return res.status(400).send('No files were uploaded.');
   
    // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
    let sampleFile = req.files.filetoupload;
   console.log(sampleFile);
   console.log(path.join(__dirname + '\\stub\\'));
    // Use the mv() method to place the file somewhere on your server
    sampleFile.mv(path.join(__dirname + '\\stub\\'+sampleFile.name), function(err) {
      if (err)
        return res.status(500).send(err);
   
      res.send('File uploaded!');
    });
});

app.get('/stub', (req, res) => {  
    res.sendFile(path.join(__dirname, 'stub/stub.json'));
});
app.get('/config', (req, res) => {  
    var obj =path.join(__dirname, 'stub/appConfig.json');
    console.log(obj);
    res.sendFile(obj);

});
//redirect everything else to the static path
app.get('*', (req, res) => {  
    res.sendFile(path.join(__dirname, 'dist/index.html'));
});

//set the port
const port = process.env.PORT || '3005';  
app.set('port', port); 

//server
const server = http.createServer(app);

//listen on the port
server.listen(port, () =>{
     console.log('API running on localhost:' + port)
        
        risorse.ciao
    }); 









