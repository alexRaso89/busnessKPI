var express = require('express');
var fs = require('fs'), path = require('path'), URL = require('url');
const router      = express.Router();
/*app.use(express.json());       
app.use(express.urlencoded()); */
var XLSX = require('xlsx');
var vistaTotali=[];
var async = require('async');
var request = require('request');
/*-------------------------- porto in memoria i dati---------------------------------- */
  var inizializzaTotali=function (){
    for (var i = 0; i < 3; i++) {
      var canale=[];
      
      data=load_data(i);
       var label=normalizeData(data,'label','this month');
      console.log(label);
       for (var j = 0; j < label.length; j++) {
        var valoricanale={};
        valoricanale.nome=label[j].label;
        
        //var element = array[index];
         
         valoricanale.valori=normalizeData(data,label[j].label,'this month'); 
         //console.log("label",label[j].label);
         canale[j]=valoricanale;
       }
       vistaTotali[i]=canale;
       //console.log(vistaTotali[i]);
    }
    /*console.log("totale",vistaTotali);
    console.log("sheet 0-------");
    console.log(vistaTotali[0]);
    console.log("sheet 0 HP-------");*/
    console.log(vistaTotali[0][1]);
  }
  module.exports = {
    inizializzaTotali: inizializzaTotali()
    
  }
  /*------------------------- router ---------------------------- */
  router.get('/res', function (req, res) {
    
    //res.writeHead(500,{});
    res.send(orchestrator('accessi',req.query.selector,req.query.timerange));
  
  });
  router.get('/accessi', function (req, res) {
    res.send(orchestrator('accessi',req.query.selector,req.query.timerange));
    //console.log(req);
  });
  router.get('/ricariche', function (req, res) {
    res.send(orchestrator('ricariche',req.query.selector,req.query.timerange));
    //console.log(req);
  });
  router.get('/attivazioni', function (req, res) {
    res.send(orchestrator('attivazioni',req.query.selector,req.query.timerange));
    //console.log(req);
  });
  router.get('/TroubleTicket', function (req, res) {
    //extractData(req.query.selector,'accessi');
    //res.writeHead(500,{});
    res.send( normalizeDataTT(load_data(filtraSheet(req.query.timerange)),req.query.selector));
     
    //console.log(req);
  });
  /*router.get('/trend/accessi', function (req, res) {
    
    res.send(getTrend(req.query.selector,load_data(8),"accessi"));

    //console.log(req);
  });
  router.get('/trend/acquisizioni', function (req, res) {
    
    res.send(getTrend(req.query.selector,load_data(8),"acquisizioni"));

    //console.log(req);
  });
  router.get('/trend/ricariche', function (req, res) {
    
    res.send(getTrend(req.query.selector,load_data(8),"ricariche"));

    //console.log(req);
  });
*/
  router.get('/trend/:risorsa', function (req, res) {
    console.log(req.params.risorsa);
    res.send(getTrend(req.query.selector,load_data(8),req.params.risorsa));
  });
  router.get('/dashTable', function (req, res) {
    console.log(req.params.risorsa);
    getdashTrend(load_data(8),function(err,data){
        res.send(data);
    });
  });
  router.get('/aaa', function (req, res) {
    console.log(req.params.risorsa);
    /*getdashTrendNew(function(err,data){
        res.send(data);
    });*/
   // getdashTrendNew(res);
  // res.send(trendasy());
  trendasy(res);
  });
  module.exports = router;
  /*--------------------------------------------------------------------------------------- */
/*-----------------------------------------orchestratore---------------------------------------------- */
function orchestrator(risorsa,selettore,timerange){
  if (selettore=="totali") {
    return calcolaTotale(filtraSheet(risorsa),timerange);
  }else{
    return timeFilter(filtraCanale(filtraSheet(risorsa),selettore),timerange);
  }
}
/**-----------------------------------------filtro per canale------------------------------------------------------------ */
function filtraCanale(contenutoSheet, canale){
  for (var i = 0; i < contenutoSheet.length; i++) {
    if (contenutoSheet[i].nome==canale) {
      return contenutoSheet[i].valori
    }
    
  }
}
/**-----------------------------------------filtro per sheet------------------------------------------------------------ */

function filtraSheet(sheet){
  var data;  
  switch (sheet) {
    
      case "accessi":
        
        return vistaTotali[0];
        break;
      case "attivazioni":
        return vistaTotali[2];
        break;
      case "ricariche":
        return vistaTotali[1];
        break;
        /**gestione per TT di dettaglio*/
        case "totali":
        return 3
        break;
        case "Business e Open Access":
          return 4
          break;
        case "Consumer Fisso":
          return 5
          break;
        case "Consumer Mobile":
          return 6
          break;
        case "Mail e servizi VAS":
          return 7
          break;
      default:
        break;
    }
}
/**-----------------------------------------Calcolo i totali per pie chart------------------------------------------------------------ */

function calcolaTotale(contenutoSheet,timerange){
          
    var text = '[';
    for (var i = 0; i < contenutoSheet.length; i++) {
      text +='{ "label":"'+contenutoSheet[i].nome+'" , "value":"'+timeTotal(contenutoSheet[i].valori,timerange)+'" },';
        
      }
      var finalText=text.substring(0, text.length - 1);
      finalText +=']';
      
      return JSON.parse(finalText);
  }

/**-----------------------------------------lettura file excel------------------------------------------------------------ */
  function load_data(sheet) {
    //var buf = fs.readFileSync("sheetjs.xlsx");
    //var wb = XLSX.read(buf, {type:'buffer'});
    var wb = XLSX.readFile("stub/prova.xlsx");
    /* generate array of arrays */
    data = XLSX.utils.sheet_to_json(wb.Sheets[wb.SheetNames[sheet]], {header:1});
    //console.log(data);
    return data;
  }
/**------------------normalizzo i dati viene richiamata nell'inizializzazione------------------------- */
  function normalizeData(data,selector,timerange){
    
    //console.log("selector:",selector);
    if(selector=="label" ){
      var text = '[';
      for(var i=1;i<data.length;i++){
        if(data[i][0]!=undefined){
            if(timerange=="this month")
              text +='{ "label":"'+data[i][0]+'"},';
           
        }
      }
      var finalText=text.substring(0, text.length - 1);
      finalText +=']';
      
      return JSON.parse(finalText);
    }else{
      var text = '[';
      for(var i=1;i<data.length;i++){
        //console.log(data[i][0]);
        if(data[i][0]==selector){
          //console.log("entro nel bast");
          for(var j=2;j<data[i].length;j++){
            text +='{ "label":"'+data[0][j]+'" , "value":"'+data[i][j]+'", "color":"#FFFFFF" },';
          }
            
        }
       
      }
      //console.log(text);
      var finalText=text.substring(0, text.length - 1);
      finalText +=']';
      //console.log(finalText);
      return JSON.parse(finalText);
    


    }

  }

  function normalizeDataTT(data,selector){
    
    //console.log("selector:",data);
    if(selector=="totali" ){
      var text = '[';
      for(var i=1;i<data.length;i++){
        if(data[i][0]!=undefined){
          
            text +='{ "label":"'+data[i][0]+'" , "value":"'+data[i][1]+'" },';
        
      }
    }
      var finalText=text.substring(0, text.length - 1);
      finalText +=']';
      
      return JSON.parse(finalText);
    }else{
      var text = '[';
      for(var i=1;i<data.length;i++){
        //console.log(data[i][0]);
        if(data[i][0]==selector){
          //console.log("entro nel bast");
          for(var j=2;j<data[i].length;j++){
            text +='{ "label":"'+data[0][j]+'" , "value":"'+data[i][j]+'", "color":"#FFFFFF" },';
          }
            
        }
       
      }
      console.log(text);
      var finalText=text.substring(0, text.length - 1);
      finalText +=']';
      //console.log(finalText);
      return JSON.parse(finalText);
    


    }

  }
  function timeTotal(oggetto,tipofiltro){
    var totale=0;
    var t=0;
     var d=new Date();
     var m=d.getMonth()+1;
     console.log(tipofiltro);
     if(tipofiltro !=="this month" ){
       if(m==1) m=12;
       else m--;
       
     }
     
     for(var i=0;i<oggetto.length;i++){
        
         var dataOggetto=new Date(oggetto[i].label);
         var meseOggetto=dataOggetto.getMonth()+1;
         
         console.log(meseOggetto);
        // console.log(oggetto[i].label);
         
         if(m==meseOggetto){
           console.log("oggetto"+parseInt(oggetto[i].value));
          totale=totale+parseInt(oggetto[i].value);
          console.log("totale"+totale);
             
         }
     }
     console.log("totale",totale);
     return totale;
   }

  function timeFilter(oggetto,tipofiltro){
   var oggettocpy=oggetto.slice();
    var d=new Date();
    var m=d.getMonth()+1;
    //console.log(m);
    if(tipofiltro !=="this month" ){
      if(m==1) m=12;
      else m--;
      
    }
    if(tipofiltro =="last week" ){
      for (var index = (oggettocpy.length-8); index >=0; index--) {
        oggettocpy.splice(index,1);
        
      }
      return oggettocpy;
    }
    if(tipofiltro =="previous week" ){
      for (var index = (oggettocpy.length-15); index >=0; index--) {
        oggettocpy.splice(index,1);
        
      }
      console.log(oggettocpy);
      for (var index = 7; index <oggettocpy.length; index++) {
        oggettocpy.splice(index,1);
        index--;
      }
      return oggettocpy;
    }
    for(var i=0;i<oggettocpy.length;i++){

        var dataOggetto=new Date(oggettocpy[i].label);
        var meseOggetto=dataOggetto.getMonth()+1;
        
       // console.log(meseOggetto);
       // console.log(oggetto[i].label);
        
        if(m!=meseOggetto){
          console.log("entro");
          oggettocpy.splice(i,1);
            i--;
        }
    }
    console.log(oggettocpy);
    return oggettocpy;
  }
 function getdashTrend(data,callback){
  
  var subObj={"acquisizioni":[], "ricariche":[]}; 
  var retObj={"acquisizioni":[], "ricariche":[]};
  for (var i = 1; i < data.length; i++) {
    
    var obj={"v1":String,"v2":String,"v3":String, "v4":String};  
    var obj1={"v1":String,"v2":String,"v3":String, "v4":String};  
    if ((data[i][0]=="acquisizioni")&&(data[i][1]!="total")) {
      
      obj["v1"]=data[i][1]; 
      if (data[i][2]=="lv") {
          obj["v3"]=data[i][3];
          subObj["acquisizioni"].push(obj)
          
        }
        
        
      
    }if ((data[i][0]=="acquisizioni")&&(data[i][1]=="total")&& (data[i][2]=="lv")) {
      var total=data[i][3];
      console.log(total);
    }
    if ((data[i][0]=="ricariche")&&(data[i][1]!="total")) {
      
      obj1["v1"]=data[i][1]; 
      if (data[i][2]=="lv") {
          obj1["v3"]=data[i][3];
          subObj["ricariche"].push(obj1)
          
        }
   
        
      
    }if ((data[i][0]=="ricariche")&&(data[i][1]=="total")&& (data[i][2]=="lv")) {
      var total1=data[i][3];
      console.log(total1);
    }

}

subObj["ricariche"][0]["v4"]=total1;
subObj["acquisizioni"][0]["v4"]=total;
request.get({url:"https://mbi.tim.it/easymonitorbi/recharge?selector=pie&timerange=lastweek"},  (error, response, body)=> {
  console.log(body);
  var ricariche=JSON.parse(body);

  request.get({url:"https://mbi.tim.it/easymonitorbi/newline?selector=pie&timerange=lastweek"},  (error, response, body)=> {
    
    
    var newline=JSON.parse(body);
    
    for (var i = 0; i < subObj["ricariche"].length; i++) {
      
      var canaleR=ricariche["v1"].indexOf(subObj["ricariche"][i]["v1"]);
      
      subObj["ricariche"][i]["v2"]=ricariche["v2"][canaleR];
      if(subObj["ricariche"][i]["v2"]!=0) subObj["ricariche"][i]["v2"]=(subObj["ricariche"][i]["v2"]/7).toFixed(0);
      console.log(subObj);
    
    }
    for (var i = 0; i < subObj["acquisizioni"].length; i++) {
      
      var canaleA=newline["v1"].indexOf(subObj["acquisizioni"][i]["v1"]);
      subObj["acquisizioni"][i]["v2"]=newline["v2"][canaleA];
      if(subObj["acquisizioni"][i]["v2"]!=0) subObj["acquisizioni"][i]["v2"]=(subObj["acquisizioni"][i]["v2"]/7).toFixed(0);
      
    }
    return callback(false,subObj);
    
  });
});

 

 }
 function getTotalTrend(data){
  
}
function getTrend(selector,data, risorsa){
  var obj={"v1":[],"v2":[],"v3":[]};
  
  for (var i = 1; i < data.length; i++) {
      if ((data[i][0]==risorsa)&&(data[i][1]==selector)) {
          if (data[i][2]=="sd") {
            obj["v3"]=data[i].slice(4,data[i].length);  
          }else{
            obj["v1"]=data[0].slice(4,data[i].length);
            
            obj["v2"]=data[i].slice(4,data[i].length);
          }
          
      }
      
  }
  return obj;   
}
function trendasy(res){
  var subObj={ "ricariche":[],"acquisizioni":[]}; 
  var tasks=[
   function(callback){ 
    trendasyR(function(err,res){
      console.log("res",res);
      return callback (null,res);
    })
    
  }
    ,
    function(callback){
      trendasyN(function(err,res){
        console.log("res",res);
        return callback (null,res);
      });
  }
  ];
  async.series(tasks, (err, results) => {
    if (err) {
        return next(err);
    }
    console.log(results[0][0]["v2"]);
    var totalR=results[0][0]["v2"];
    var totalN=results[1][0]["v2"];
    results[0][1]["v4"]=totalR;
    results[1][1]["v4"]=totalN;

    subObj["ricariche"]=results[0].slice(1,results[0].length);
    subObj["acquisizioni"]=results[1].slice(1,results[1].length);
    res.send(subObj);   
})
}
function trendasyN(callback){
  
    //console.log(acq());
    //console.log(ric());
   var tasks=[
      function (callback) {
        //Stats.getMemoryUsage(callback)
        request.get("https://mbi.tim.it/easymonitorbi/newline?selector=pie&timerange=lastweek",function(error, response, body) {
          console.log(error);
          if(error) callback("servizio non disponibile",null);
          var newline=JSON.parse(body);
          callback(null,newline);
           
            
        });
        console.log("esco");
        
        
      },
      function (newline,callback) {
        //Stats.getMemoryUsage(callback)
         var piuAll=['all'];
         var arr=piuAll.concat(newline['v1']);
         console.log("piuall",arr);
         var total=0;
        async.map(arr, function(selettore, callback) {
          request.get("https://mbi.tim.it/easymonitorbi/newline?selector="+selettore+"&timerange=trend",function(error, response, body) {
            console.log(body);
           
            if(error) callback("servizio non disponibile",null);
            var trend=JSON.parse(body);
            var obj={"v1":[],"v2":[],"v3":[]};
            if(selettore!="all"){
              obj["v1"]=selettore;
              obj["v2"]=(newline["v2"][newline["v1"].indexOf(selettore)]/7).toFixed(0);
              obj["v3"]=trend["v2"][trend['v2'].length-1];
              obj["v4"]=total;
              callback(null,obj); 
            }
            if(selettore=="all"){
              obj["v1"]=selettore;
              obj["v2"]=trend["v2"][trend["v2"].length-1]; 
              callback(null,obj);
            }
                 
          });


        }, function(err, obj) {
            console.log("trend",obj);
            callback(null,obj);
        });
        
        
      }

    ];
   async.waterfall(tasks, (err, results) => {
      if (err) {
        callback(err,null);  
      }
      console.log(results)
      callback(null,results);    
  })
  

  
}
function trendasyR(callback){
  
    //console.log(acq());
    //console.log(ric());
    
   var tasks=[
      function (callback) {
        //Stats.getMemoryUsage(callback)
        request.get("https://mbi.tim.it/easymonitorbi/recharge?selector=pie&timerange=lastweek",function(error, response, body) {
          console.log(error);
          if(error) callback("servizio non disponibile",null);
          var newline=JSON.parse(body);

           callback(null,newline);
            
        });
        console.log("esco");
        
        
      },
      function (newline,callback) {
        //Stats.getMemoryUsage(callback)
         var piuAll=['all'];
         var arr=piuAll.concat(newline['v1']);
         console.log("piuall",arr);
         var total=0;
        async.map(arr, function(selettore, callback) {
          request.get("https://mbi.tim.it/easymonitorbi/recharge?selector="+selettore+"&timerange=trend",function(error, response, body) {
            console.log(error);
           
            if(error) callback("servizio non disponibile",null);
            var trend=JSON.parse(body);
            
            var obj={"v1":[],"v2":[],"v3":[]};
            if(selettore!="all"){
              obj["v1"]=selettore;
              obj["v2"]=(newline["v2"][newline["v1"].indexOf(selettore)]/7).toFixed(0);
              obj["v3"]=trend["v2"][trend['v2'].length-1];
              obj["v4"]=total;
              callback(null,obj); 
            }
            if(selettore=="all"){
              obj["v1"]=selettore;
              obj["v2"]=trend["v2"][trend["v2"].length-1]; 
              callback(null,obj);
            }
                 
          });


        }, function(err, obj) {
            console.log("trend",obj);
            callback(null,obj);
        });
        
        
      }

    ];
   async.waterfall(tasks, (err, results) => {
      if (err) {
          return next(err);
      }
      console.log(results);
      callback(null,results);    
  })
  

  
}
function getdashTrendNew(res){
  var subObj={"acquisizioni":[], "ricariche":[]}; 
  var retObj={"acquisizioni":[], "ricariche":[]};
  var obj={"v1":String,"v2":String,"v3":String, "v4":String};
  async.series([
    function(callback) {
      //Stats.getMemoryUsage(callback)
      request.get({url:"http://timbusinesskpi.azurewebsites.net/mbi/easymonitorbi/newline?selector=pie&timerange=lastweek"},(error, response, body)=> {
        callback(null,body);
      });



    },
    function(callback) {
      //Stats.getCPUUsage(callback)
      async.series([
        function(callback) {
          request.get({url:"http://timbusinesskpi.azurewebsites.net/mbi/easymonitorbi/recharge?selector=pie&timerange=lastweek"},(error, response, body)=> {
            console.log(JSON.parse(body));
            callback(null,JSON.parse(body));
          });
        },
        function(ricarichePie, callback) {
          console.log("ric",ricarichePie);
          var objs=[];
          objs= ricarichePie['v1'].filter((canale,index)=>{
            request.get({url:"http://timbusinesskpi.azurewebsites.net/mbi/easymonitorbi/recharge?selector="+canale+"&timerange=trend"},(error, response, body)=> {
                var trend=JSON.parse(body)
                var obj={"v1":String,"v2":String,"v3":String, "v4":String};
                obj['v1']=canale;
                obj['v2']=ricarichePie['v2'][index]/7;
                obj['v3']=trend['v2'][trend['v2'].length-1];
                console.log(obj);
                return obj;
            });
          })
          console.log("objs",objs);
        callback(null,objs)
        }
      ],
      function(err, objs) {
        console.log(objs);
       //callback(null,objs);
      })
   
    }
  ],
  function(err, results) {
    

    res.send( {memory: results[0], cpu: results[1]})
  })
  
  /*request.get({url:"http://timbusinesskpi.azurewebsites.net/mbi/easymonitorbi/newline?selector=pie&timerange=lastweek"},  (error, response, body)=> {
    var newline=JSON.parse(body);
    
    for (var i = 0; i < newline["v1"].length; i++) {
        console.log(newline["v1"][i]);
        obj["v1"]=newline["v1"][i];
        obj["v2"]=newline["v2"][i]/7;
        request.get({url:"http://timbusinesskpi.azurewebsites.net/mbi/easymonitorbi/newline?selector="+newline["v1"][i]+"&timerange=trend"},  (error, response, body1, call)=> {
          var trend=JSON.parse(body1);
          console.log(trend["v2"][trend["v2"].length-1]);
          obj["v3"]=trend["v2"][trend["v2"].length-1];
          callback(false,obj)
        });      


    }
    return callback(false,obj);
  });*/


}

function acq(callback) {
  //Stats.getMemoryUsage(callback)
  request.get({url:"https://mbi.tim.it/easymonitorbi/newline?selector=pie&timerange=lastweek"},(error, response, body)=> {
    console.log(body);
    callback(null,body);
  });

  //callback(null,"1");
}
function ric(callback) {
  //Stats.getCPUUsage(callback)
  request.get({url:"https://mbi.tim.it/easymonitorbi/recharge?selector=pie&timerange=lastweek"},(error, response, body)=> {
     return callback(null,body);
  });
}
/*function trendasy(res) {
  
  console.log("entro");
  async.waterfall([
    function(callback) {

      
      callback(null,request.get({url:"http://timbusinesskpi.azurewebsites.net/mbi/easymonitorbi/recharge?selector=pie&timerange=lastweek"},(error, response, body)=> {
        console.log(JSON.parse(body));
        var ricarichePie=JSON.parse(body);
         return ricarichePie;
      }));
    // callback(null,ricarichePie);
    },

  ],
  function(err, succ) {
    console.log(succ);
   //callback(null,objs);
   res.send(succ);
  })
}*/